package main

import (
	"context"
	"fmt"
	"github.com/antvirgeo/tcp-pow/config"
	"github.com/antvirgeo/tcp-pow/internal/server"
	"github.com/antvirgeo/tcp-pow/pkg/cache"
	"github.com/antvirgeo/tcp-pow/pkg/context"
	"github.com/antvirgeo/tcp-pow/pkg/customerrors"
	log "github.com/sirupsen/logrus"
	"math/rand"
	"time"
)

func main() {
	logger := log.WithField("component", "server")
	logger.Info("application started")

	cfg := config.Read()

	// random form quotes (rand.Seed deprecated in 1.20)
	seed := rand.NewSource(time.Now().UnixNano())
	rnd := rand.New(seed)

	ctx := context.Background()
	ctx = context.WithValue(ctx, cntxt.Config, cfg.App)
	ctx = context.WithValue(ctx, cntxt.Random, rnd)

	cacheRedis, err := cache.InitRedisCache(ctx, cfg.App.CacheHost, cfg.App.CachePort)
	if err != nil {
		logger.WithError(err).Error("failed init redis cache")
		return
	}
	ctx = context.WithValue(ctx, cntxt.Cache, cacheRedis)

	serverAddress := fmt.Sprintf("%s:%d", cfg.App.ServerHost, cfg.App.ServerPort)

	repo := server.NewQuotesRepository()
	srv := server.NewServer(repo)
	if err = srv.Run(ctx, serverAddress); err != nil {
		switch err {
		//possible useless switch logs, cause of detailed customError text in that project, and one union log for error is enough here
		case customerrors.ErrSrvListen, customerrors.ErrSrvConnectionAccept:
			//donno, mb need to do something with this particular errors
			logger.WithError(err).WithFields(log.Fields{"reason": err.Error()}).
				Warn("application failed to run")
		default:
			logger.WithError(err).WithFields(log.Fields{"reason": "unexpected"}).
				Error("application failed to run")
		}

		return
	}
}
