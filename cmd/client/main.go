package main

import (
	"context"
	"fmt"
	"github.com/antvirgeo/tcp-pow/config"
	"github.com/antvirgeo/tcp-pow/internal/client"
	"github.com/antvirgeo/tcp-pow/pkg/context"
	"github.com/antvirgeo/tcp-pow/pkg/customerrors"
	log "github.com/sirupsen/logrus"
)

func main() {
	logger := log.WithField("component", "client")
	logger.Info("application started")

	cfg := config.Read()

	ctx := context.Background()
	ctx = context.WithValue(ctx, cntxt.Config, cfg.App)

	serverAddress := fmt.Sprintf("%s:%d", cfg.App.ServerHost, cfg.App.ServerPort)

	cln := client.NewClient()
	if err := cln.Run(ctx, serverAddress); err != nil {
		switch err {
		case customerrors.ErrClntUnexpectedError:
			logger.WithError(err).WithFields(log.Fields{"reason": err.Error()}).
				Warn("application failed to run")
		default:
			logger.WithError(err).WithFields(log.Fields{"reason": "unexpected"}).
				Error("application failed to run")
		}

		return
	}
}
