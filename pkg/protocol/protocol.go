package protocol

import (
	"github.com/antvirgeo/tcp-pow/pkg/pow"
)

// challenge-response protocol via https://en.wikipedia.org/wiki/Proof_of_work

type MessageType int

const (
	Exit              MessageType = iota // close connections
	RequestChallenge                     // client request new challenge from server
	ResponseChallenge                    // server answer client with challenge
	RequestAnswer                        // client send solved challenge server to verify
	ResponseAnswer                       // server response client with quote for passing or send error

	MagicTCPDelim = "|*|"
)

type Message struct {
	MessageType   MessageType       `json:"header"`
	ChallengeData pow.ChallengeData `json:"challenge_data,omitempty"`
	Quote         string            `json:"quote,omitempty"`
}
