package cache

type MockCache struct {
	cache map[string][]byte
}

func NewMockCache() *MockCache {
	return &MockCache{cache: make(map[string][]byte)}
}

func (mc *MockCache) Add(key string, expiration int64) error {
	return nil
}

func (mc *MockCache) Get(key string) (bool, error) {
	return true, nil
}

func (mc *MockCache) Delete(key string) {
}
