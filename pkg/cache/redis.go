package cache

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"time"
)

type RedisCache struct {
	ctx    context.Context
	client *redis.Client
}

func InitRedisCache(ctx context.Context, host string, port int) (*RedisCache, error) {
	rdb := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%d", host, port),
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	// checking connection just for case, in prod we probably need some periodic healthcheck
	err := rdb.Set(ctx, "testKey", "vah", 0).Err()
	defer rdb.Del(ctx, "testKey")

	return &RedisCache{
		ctx:    ctx,
		client: rdb,
	}, err
}

func (c *RedisCache) Add(key string, expiration int64) error {
	return c.client.Set(c.ctx, key, "notImportant", time.Duration(expiration)*time.Second).Err()
}

func (c *RedisCache) Get(key string) (bool, error) {
	val, err := c.client.Get(c.ctx, key).Result()
	return val != "", err
}

// redis key auto deletes themselves after expiration time, so we dont need to worry about collisions not deleted clients signatures

func (c *RedisCache) Delete(key string) {
	c.client.Del(c.ctx, key)
}
