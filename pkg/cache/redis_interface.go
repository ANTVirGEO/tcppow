package cache

// Cache - interface for add, delete and check existence of rand values for hashcash
type Cache interface {
	// Add - add client signature with expiration (in seconds) to cache
	Add(string, int64) error
	// Get - check existence of user signature in cache
	Get(string) (bool, error)
	// Delete - delete key from cache
	Delete(string)
}
