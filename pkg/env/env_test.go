package env

import (
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestGetEnvString(t *testing.T) {
	n := "TEST_STRING1"
	v := "test123"

	os.Setenv(n, v)
	defer os.Unsetenv(n)

	require.Equal(t, v, GetEnvString(n, ""))
}

func TestGetEnvStringDefault(t *testing.T) {
	n := "TEST_STRING2"
	v := "test123"

	require.Equal(t, v, GetEnvString(n, v))
}

func TestGetEnvBool1(t *testing.T) {
	n := "TEST_BOOL1"

	os.Setenv(n, "1")
	defer os.Unsetenv(n)

	require.Equal(t, true, GetEnvBool(n, false))
}

func TestGetEnvBoolYes(t *testing.T) {
	n := "TEST_BOOL2"

	os.Setenv(n, "yes")
	defer os.Unsetenv(n)

	require.Equal(t, true, GetEnvBool(n, false))
}

func TestGetEnvBoolTrue(t *testing.T) {
	n := "TEST_BOOL3"

	os.Setenv(n, "true")
	defer os.Unsetenv(n)

	require.Equal(t, true, GetEnvBool(n, false))
}

func TestGetEnvBoolY(t *testing.T) {
	n := "TEST_BOOL4"

	os.Setenv(n, "y")
	defer os.Unsetenv(n)

	require.Equal(t, true, GetEnvBool(n, false))
}

func TestGetEnvBoolDefault(t *testing.T) {
	n := "TEST_BOOL5"

	require.Equal(t, true, GetEnvBool(n, true))
}

func TestGetEnvInt(t *testing.T) {
	n := "TEST_INT1"

	os.Setenv(n, "-42")
	defer os.Unsetenv(n)

	require.Equal(t, -42, GetEnvInt(n, 0))
}

func TestGetEnvIntDefault(t *testing.T) {
	n := "TEST_INT2"

	require.Equal(t, -42, GetEnvInt(n, -42))
}

func TestGetEnvUint(t *testing.T) {
	n := "TEST_UINT1"

	os.Setenv(n, "42")
	defer os.Unsetenv(n)

	require.Equal(t, uint(42), GetEnvUint(n, 0))
}

func TestGetEnvUintDefault(t *testing.T) {
	n := "TEST_UINT2"

	require.Equal(t, uint(42), GetEnvUint(n, 42))
}

func TestGetEnvInt64(t *testing.T) {
	n := "TEST_INT641"

	os.Setenv(n, "-999999999")
	defer os.Unsetenv(n)

	require.Equal(t, int64(-999999999), GetEnvInt64(n, 0))
}

func TestGetEnvInt64Default(t *testing.T) {
	n := "TEST_INT642"

	require.Equal(t, int64(-999999999), GetEnvInt64(n, -999999999))
}

func TestGetEnvInt64Incorrect(t *testing.T) {
	n := "TEST_INT643"

	os.Setenv(n, "incorrect")
	defer os.Unsetenv(n)

	require.Equal(t, int64(-42), GetEnvInt64(n, -42))
}

func TestGetEnvUint64(t *testing.T) {
	n := "TEST_UINT641"

	os.Setenv(n, "999999999")
	defer os.Unsetenv(n)

	require.Equal(t, uint64(999999999), GetEnvUint64(n, 0))
}

func TestGetEnvUint64Default(t *testing.T) {
	n := "TEST_UINT642"

	require.Equal(t, uint64(999999999), GetEnvUint64(n, 999999999))
}

func TestGetEnvUint64Incorrect(t *testing.T) {
	n := "TEST_UINT643"

	os.Setenv(n, "incorrect")
	defer os.Unsetenv(n)

	require.Equal(t, uint64(42), GetEnvUint64(n, 42))
}

func TestGetEnvDuration(t *testing.T) {
	n := "TEST_DURATION1"

	os.Setenv(n, "42s")
	defer os.Unsetenv(n)

	require.Equal(t, 42*time.Second, GetEnvDuration(n, 0))
}

func TestGetEnvDurationDefault(t *testing.T) {
	n := "TEST_DURATION2"

	require.Equal(t, 42*time.Second, GetEnvDuration(n, 42*time.Second))
}

func TestGetEnvDurationIncorrect(t *testing.T) {
	n := "TEST_DURATION3"

	os.Setenv(n, "s20")
	defer os.Unsetenv(n)

	require.Equal(t, 42*time.Second, GetEnvDuration(n, 42*time.Second))
}
