package env

import (
	"os"
	"strconv"
	"strings"
	"time"
)

func GetEnvString(name, def string) (value string) {
	if value = os.Getenv(name); value == "" {
		value = def
	}

	return
}

func GetEnvBool(name string, def bool) bool {
	v := os.Getenv(name)
	if v != "" {
		v = strings.ToLower(v)
		return v == "1" || v == "yes" || v == "true" || v == "y"
	}

	return def
}

func GetEnvInt(name string, def int) int {
	v := GetEnvInt64(name, int64(def))
	return int(v)
}

func GetEnvUint(name string, def uint) uint {
	v := GetEnvUint64(name, uint64(def))
	return uint(v)
}

func GetEnvInt64(name string, def int64) int64 {
	v := os.Getenv(name)
	if v != "" {
		i64, err := strconv.ParseInt(v, 10, 0)
		if err != nil {
			return def
		}

		return i64
	}

	return def
}

func GetEnvUint64(name string, def uint64) uint64 {
	v := os.Getenv(name)
	if v != "" {
		i64, err := strconv.ParseUint(v, 10, 0)
		if err != nil {
			return def
		}

		return i64
	}

	return def
}

func GetEnvDuration(name string, def time.Duration) time.Duration {
	v := os.Getenv(name)
	if v != "" {
		d, err := time.ParseDuration(v)
		if err != nil {
			return def
		}

		return d
	}

	return def
}
