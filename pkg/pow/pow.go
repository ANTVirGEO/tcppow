package pow

import (
	"crypto/sha1"
	"fmt"
	"github.com/antvirgeo/tcp-pow/pkg/customerrors"
)

//TODO: probably is good idea to make few realisations: Hashcash and Ethash (for example), to switch, when it needed

const zeroByte = 48

// ChallengeData - struct with fields for Hashcash
// https://en.wikipedia.org/wiki/Hashcash
type ChallengeData struct {
	Version    int
	ZerosCount int
	Date       int64
	Resource   string
	Signature  string
	Counter    int
}

// Stringify - stringifies hashcash for next sending it on TCP
func (h ChallengeData) Stringify() string {
	return fmt.Sprintf("%d:%d:%d:%s::%s:%d", h.Version, h.ZerosCount, h.Date, h.Resource, h.Signature, h.Counter)
}

// sha1Hash - calculates sha1 hash from given string
func sha1Hash(data string) string {
	h := sha1.New()
	h.Write([]byte(data))
	bs := h.Sum(nil)
	//string()
	return fmt.Sprintf("%x", bs)
}

// IsHashCorrect - checks that hash has leading <zerosCount> zeros
func IsHashCorrect(hash string, zerosCount int) bool {
	if zerosCount > len(hash) {
		return false
	}
	for _, ch := range hash[:zerosCount] {
		if ch != zeroByte {
			return false
		}
	}
	return true
}

// SolveChallenge - calculates correct hashcash by bruteforce
// until the resulting hash satisfies the condition of IsHashCorrect
// maxIterations to prevent endless computing (0 or -1 to disable it)
func (h ChallengeData) SolveChallenge(maxIterations int) (ChallengeData, error) {
	for h.Counter <= maxIterations || maxIterations <= 0 {
		header := h.Stringify()
		hash := sha1Hash(header)
		if IsHashCorrect(hash, h.ZerosCount) {
			return h, nil
		}
		// if hash don't have needed count of leading zeros, we are increasing counter and try next hash
		h.Counter++
	}
	return h, customerrors.ErrPOWMaxIterations
}
