package pow

import (
	"encoding/base64"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
	"time"
)

type TestCheckData struct {
	ChallengeData   ChallengeData
	MaxIterations   int
	Completed       bool
	Positive        bool
	Description     string
	ExpectedCounter int
}

func Test_SolveChallenge(t *testing.T) {
	t.Parallel()

	var testData = []TestCheckData{
		{
			Description: "Test with 1 zeros",
			ChallengeData: ChallengeData{
				Version:    1,
				ZerosCount: 1,
				Date:       time.Date(2023, 8, 4, 16, 40, 0, 0, time.UTC).Unix(),
				Resource:   "128.0.0.1",
				Signature:  base64.StdEncoding.EncodeToString([]byte("27df08dd-5419-43cc-a2bf-59189e0860cd")),
			},
			ExpectedCounter: 17,
			MaxIterations:   -1,
			Positive:        true,
		},
		{
			Description: "Test with 5 zeros",
			ChallengeData: ChallengeData{
				Version:    1,
				ZerosCount: 5,
				Date:       time.Date(2023, 8, 4, 16, 45, 0, 0, time.UTC).Unix(),
				Resource:   "128.10.15.3",
				Signature:  base64.StdEncoding.EncodeToString([]byte("4e06b3ee-1cf9-4613-bf49-757ba6750492")),
			},
			ExpectedCounter: 511354,
			MaxIterations:   -1,
			Positive:        true,
		},
		{
			Description: "Impossible challenge",
			ChallengeData: ChallengeData{
				Version:    1,
				ZerosCount: 10,
				Date:       time.Date(2022, 3, 13, 2, 28, 0, 0, time.UTC).Unix(),
				Resource:   "128.0.1.1",
				Signature:  base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%d", 123459))),
			},
			MaxIterations: 10,
			Positive:      false,
		},
	}

	for _, test := range testData {
		msgOnError := fmt.Sprintf("Description: %v;\nChallengeData: %v;\nPositive: %v;\n",
			test.Description, test.ChallengeData, test.Positive)
		result, err := test.ChallengeData.SolveChallenge(test.MaxIterations)
		if test.Positive {
			assert.NoError(t, err, msgOnError)
			assert.Equal(t, test.ExpectedCounter, result.Counter, msgOnError)
		} else {
			require.Error(t, err, msgOnError)
		}
	}
}

func TestHash(t *testing.T) {
	result := sha1Hash("1:1:1691167200:128.0.0.1::MjdkZjA4ZGQtNTQxOS00M2NjLWEyYmYtNTkxODllMDg2MGNk:17")
	assert.Equal(t, "0e27ae5f871ca9b9d58967775480652646253b27", result)
	result = sha1Hash("VAH")
	assert.Equal(t, "6b45af76959d91cf6f59a229b11aeffa15684d65", result)
	result = sha1Hash("")
	assert.NotEqual(t, "", result)
}

func TestIsHashCorrect(t *testing.T) {
	result := IsHashCorrect("0000000005cb14250d7d8f60e1f80fa087979db24cb8", 4)
	assert.True(t, result)
	result = IsHashCorrect("000050d7d8f60e1f80fa087979db24cb8", 4)
	assert.True(t, result)
	result = IsHashCorrect("asdfasdfasd", 1)
	assert.False(t, result)
	result = IsHashCorrect("0", 5)
	assert.False(t, result)
	result = IsHashCorrect("0e27ae5f871ca9b9d58967775480652646253b27", 10)
	assert.False(t, result)
}
