package customerrors

import "errors"

var (
	ErrSrvListen           = errors.New("server can't be listen")
	ErrSrvConnectionAccept = errors.New("server can't accept connection")

	ErrSrvUnknownHeader             = errors.New("unknown header")
	ErrSrvChallengeExpiredOrNotSent = errors.New("challenge expired or not sent")
	ErrSrvInvalid                   = errors.New("invalid hashcash resource")

	ErrClntUnexpectedError = errors.New("unexpected empty quote without errors")

	ErrPOWMaxIterations = errors.New("max iterations exceeded")
)
