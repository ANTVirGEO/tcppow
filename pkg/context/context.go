package cntxt

type ContextType string

const (
	Config ContextType = "cfg"
	Random ContextType = "rnd"
	Clock  ContextType = "clock"
	Cache  ContextType = "cache"
)
