package config

import (
	"github.com/antvirgeo/tcp-pow/pkg/env"
	log "github.com/sirupsen/logrus"
)

type Config struct {
	Log Log
	App App
}

type Log struct {
	Level log.Level
}

type App struct {
	Environment   string
	ServerHost    string
	ServerPort    int
	ZerosCount    int
	Duration      int64
	MaxIterations int
	CacheHost     string
	CachePort     int
}

func Read() Config {
	// config read from env, that simplify it changes on production (possibly overkill for this task, cause never will be in production)
	logLevel, err := log.ParseLevel(env.GetEnvString("TCPPOW_LOG_LEVEL", "debug"))
	if err != nil {
		logLevel = log.ErrorLevel
	}

	cfg := Config{
		Log: Log{
			Level: logLevel,
		},
		App: App{
			Environment:   env.GetEnvString("TCPPOW_ENV", "local"),
			ServerHost:    env.GetEnvString("TCPPOW_SERVER_HOST", "localhost"),
			ServerPort:    env.GetEnvInt("TCPPOW_SERVER_PORT", 9999),
			ZerosCount:    env.GetEnvInt("TCPPOW_ZEROS_COUNT", 3),
			Duration:      env.GetEnvInt64("TCPPOW_DURATION", 300),
			MaxIterations: env.GetEnvInt("TCPPOW_MAX_ITERATIONS", 1000000),
			CacheHost:     env.GetEnvString("TCPPOW_CACHE_HOST", "localhost"),
			CachePort:     env.GetEnvInt("TCPPOW_CACHE_PORT", 6379),
		},
	}

	return cfg
}
