# Build app
FROM golang:1.20 AS build

WORKDIR /build

COPY .. .

RUN go mod tidy && go mod download

RUN GO111MODULE=on CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o app ./cmd/client

# Copy all
FROM scratch

COPY --from=build /build/app /

ENTRYPOINT ["/app"]
