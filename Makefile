install:
	go mod tidy && go mod download

test:
	go clean --testcache
	go test ./... -coverprofile=coverage.out

start-server:
	go run cmd/server/main.go

start-client:
	go run cmd/client/main.go

start-docker:
	docker-compose up --abort-on-container-exit --force-recreate --build server --build client