# tcpPOW

## 1. Description

Design and implement “Word of Wisdom” tcp server.  
• TCP server should be protected from DDOS attacks with the Prof of Work (https://en.wikipedia.org/wiki/Proof_of_work), the challenge-response protocol should be used.  
• The choice of the POW algorithm should be explained.  
• After Prof Of Work verification, server should send one of the quotes from “word of wisdom” book or any other collection of the quotes.  
• Docker file should be provided both for the server and for the client that solves the POW challenge

## 2. Getting started

### 2.1 Launch tests:
```
make test
```

### 2.2 Start only server:
```
make start-server
```

### 2.3 Start only client:
```
make start-client
```

### 2.4 Start server and client by docker-compose:
```
make start-docker
```

## 3. Structure of the project
Standard go unofficial layout.
Existing directories:
+ cmd/client - main.go for client
+ cmd/server - main.go for server
+ deploy - docker configs for client and server
+ config - configuration info and selecting it from ENV
+ internal/client - all logic of client
+ internal/server - all logic of server (probably best do for production is put it in middleware)
+ pkg/cache - cache for redis using
+ pkg/context - some prevention from using magic words into context
+ pkg/customerrors - one place for all custom errors
+ pkg/env - logic for parsing ENV for config
+ pkg/pow - logic of chosen PoW algorithm (Hashcash)
+ pkg/protocol - constants and struct for protocol


### 4 Selection of an algorithm
There is some different algorithms of Proof Work and their pros and cons:

**Equihash**:
+ Advantages: high degree of security, protection against DDoS attacks.
+ Disadvantages: Requires a lot of memory to perform calculations.

**Bcrypt**:
+ Benefits: Fewer computing resources, faster block generation time, DDoS protection.
+ Disadvantages: Less secure than SHA-256.

**X11**:
+ Benefits: More difficult to attack, DDoS protection.
+ Disadvantages: May require a large amount of computing resources.

**Cryptonight**:
+ Advantages: high degree of security, protection against DDoS attacks.
+ Disadvantages: Requires a lot of memory to perform calculations.

**Blake2b**:
+ Advantages: fewer computing resources, protection against DDoS attacks.
+ Disadvantages: Less secure than SHA-256.
___
After comparison, I chose **Hashcash** with SHA-256, because it has already good working solutions on it

**Pros:**
+ DDoS Protection: Hashcash allows you to protect your server from DDoS attacks by requiring clients to perform a computationally demanding task before establishing a connection. This creates a barrier for attackers who are trying to overload the server with fake traffic.
+ Ease of Implementation: The Hashcash algorithm is relatively easy to implement and can be used in a variety of applications including email and network security.

**Cons:**
+ Computational Costs: Although the Hashcash algorithm is effective in protecting against DDoS attacks, it requires significant computational resources to perform the calculations. This can be a problem for clients with limited computing power or devices with limited resources.
+ Attackability: Although the Hashcash algorithm provides protection against DDoS attacks, it is not ideal and may be susceptible to some types of attacks, such as brute-force attacks or botnet attacks.


## 5. Ways to improve
1. Write integrations tests for DDoS-attacks to get some metrics
2. Implement another algorithm (_Ethash_) to check metrics comparison for various product situations
3. Move server/client from Internal to Middleware for future easy implementation to other projects
4. Add more security layer to client\server messaging? (some addition hash with salt maybe)

## 9000. PS

_Other methods have also been used to protect the TCP server from DDOS attacks, such as firewalls, access control lists (ACLs), and distributed denial of protection (DDoS) attack protection services such as AWS Shield (no ads :)). You can also use scaling planning to ensure that you have sufficient server performance to absorb and mitigate attacks._