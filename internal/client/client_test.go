package client

import (
	"bufio"
	"context"
	"errors"
	"fmt"
	"github.com/antvirgeo/tcp-pow/config"
	cntxt "github.com/antvirgeo/tcp-pow/pkg/context"
	"github.com/antvirgeo/tcp-pow/pkg/customerrors"
	"github.com/antvirgeo/tcp-pow/pkg/protocol"
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

// MockConnection - mocks tcp connection by two interfaces (reader and writer) and funcs
type MockConnection struct {
	ReadFunc  func([]byte) (int, error)
	WriteFunc func([]byte) (int, error)
}

func (m MockConnection) Read(p []byte) (n int, err error) {
	return m.ReadFunc(p)
}

func (m MockConnection) Write(p []byte) (n int, err error) {
	return m.WriteFunc(p)
}

func getMockServer() (*Client, context.Context) {
	cfg := config.Read()

	ctx := context.Background()
	ctx = context.WithValue(ctx, cntxt.Config, cfg.App)

	return NewClient(), ctx
}

type TestCheckData struct {
	ClientMessage string
	ClientAddress string
	Connection    MockConnection
	BufferReader  *bufio.Reader
	Completed     bool
	Positive      bool
	Description   string
	ExpectedError error
}

func TestHandleConnection(t *testing.T) {
	t.Parallel()

	clnt, ctx := getMockServer()

	var testData = []TestCheckData{
		{
			Description: "Step 1. Simulate error on send request for challenge to server",
			Connection: MockConnection{
				WriteFunc: func(p []byte) (int, error) {
					return 0, errors.New("test write error")
				},
			},
			Positive: false,
		},
		{
			Description: "Step 1. Bad format message with challenge from server",
			Connection: MockConnection{
				WriteFunc: func(p []byte) (int, error) {
					return 0, nil
				},
			},
			BufferReader: bufio.NewReader(strings.NewReader("VAH")),
			Positive:     false,
		},
		{
			Description: "Step 1. Bad format json message with challenge from server",
			Connection: MockConnection{
				WriteFunc: func(p []byte) (int, error) {
					return 0, nil
				},
			},
			BufferReader: bufio.NewReader(strings.NewReader(fmt.Sprintf(
				`{"header":%d,"challenge_data%s`,
				protocol.RequestAnswer, "\n"))),
			Positive: false,
		},
		{
			Description: "Step 2. Empty challenge from server",
			Connection: MockConnection{
				WriteFunc: func(p []byte) (int, error) {
					return 0, nil
				},
			},
			BufferReader: bufio.NewReader(strings.NewReader(fmt.Sprintf(
				`{"header":%d,"challenge_data":{}}%s`,
				protocol.RequestAnswer, "\n"))),
			Positive: false,
		},
		{
			Description: "Step 2. Impossible zero count",
			Connection: MockConnection{
				WriteFunc: func(p []byte) (int, error) {
					return 0, nil
				},
			},
			BufferReader: bufio.NewReader(strings.NewReader(fmt.Sprintf(
				`{"header":%d,"challenge_data":{"Version":1,"ZerosCount":100,"Date":1691169914,"Resource":"127.0.0.1:57226","Signature":"ZDViYTVmYjktY2IyOC00YTYzLWEwODgtZjNlYjFiYzg3NzUx","Counter":0}}%s`,
				protocol.RequestAnswer, "\n"))),
			Positive:      false,
			ExpectedError: customerrors.ErrPOWMaxIterations,
		},
		//3 step already tested in 1, just sending
		{
			Description: "Step 4. Accept quote",
			Connection: MockConnection{
				WriteFunc: func(p []byte) (int, error) {
					return 0, nil
				},
			},
			BufferReader: bufio.NewReader(strings.NewReader(fmt.Sprintf(
				`{"header":%d,"challenge_data":{"Version":1,"ZerosCount":3,"Date":1691169914,"Resource":"127.0.0.1:57226","Signature":"ZDViYTVmYjktY2IyOC00YTYzLWEwODgtZjNlYjFiYzg3NzUx","Counter":0}}%s{"header":%d,"quote":"Blessed is the mind too small for doubt. - Leman Russ"}%s`,
				protocol.RequestAnswer, "\n", protocol.ResponseAnswer, "\n"))),
			Positive: true,
		},
	}

	for _, test := range testData {
		msgOnError := fmt.Sprintf("Description: %v;\nClientMessage: %v;\nPositive: %v;\n",
			test.Description, test.ClientMessage, test.Positive)
		_, err := clnt.HandleConnection(ctx, test.BufferReader, test.Connection)
		if test.Positive {
			assert.NoError(t, err, msgOnError)
		} else {
			assert.Error(t, err, msgOnError)
		}
	}
}
