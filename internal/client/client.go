package client

import (
	"bufio"
	"context"
	"encoding/json"
	"github.com/antvirgeo/tcp-pow/config"
	"github.com/antvirgeo/tcp-pow/pkg/context"
	"github.com/antvirgeo/tcp-pow/pkg/customerrors"
	"github.com/antvirgeo/tcp-pow/pkg/protocol"
	log "github.com/sirupsen/logrus"
	"io"
	"net"
)

type Client struct {
	logger *log.Entry
}

func NewClient() *Client {
	return &Client{
		logger: log.WithField("component", "client"),
	}
}

// Run client connection to server for verification and getting quote
func (c *Client) Run(ctx context.Context, address string) error {
	srvCon, err := net.Dial("tcp", address)
	if err != nil {
		return err
	}
	defer srvCon.Close()

	c.logger.WithFields(log.Fields{"address": address}).Info("client connected to server")

	reader := bufio.NewReader(srvCon)

	quote, err := c.HandleConnection(ctx, reader, srvCon)
	if err != nil {
		return err
	}

	if quote != "" {
		c.logger.WithFields(log.Fields{"quote": quote}).Info("finish! quote is arrived")
	} else {
		return customerrors.ErrClntUnexpectedError
	}

	return nil
}

// HandleConnection - scenario for TCP-client
// Step 1. request challenge from server
// Step 2. resolve challenge
// Step 3. send resolved challenge solution back to server
// Step 4. get result quote from server
func (c *Client) HandleConnection(ctx context.Context, reader *bufio.Reader, conWriter io.Writer) (string, error) {
	// Step 1. requesting challenge from server
	request := protocol.Message{
		MessageType: protocol.RequestChallenge,
	}
	marshalledRequest, err := json.Marshal(request)
	if err != nil {
		c.logger.WithError(err).WithField("step", 1).Error("failed to marshal start request")
		return "", err
	}

	err = c.sendMsg(marshalledRequest, conWriter)
	if err != nil {
		c.logger.WithError(err).WithField("step", 1).Error("failed to send message for challenge request")
		return "", err
	}

	msgStr, err := c.readConnMsg(reader)
	if err != nil {
		c.logger.WithError(err).WithField("step", 1).Error("failed to read message from server")
		return "", err
	}

	msg := protocol.Message{}
	err = json.Unmarshal([]byte(msgStr), &msg)
	if err != nil {
		c.logger.WithError(err).WithField("step", 1).Error("failed to unmarshal message from server")
		return "", err
	}

	c.logger.WithFields(log.Fields{"challengeData": msg.ChallengeData, "step": 1}).Info("ChallengeData from server received")

	// Step 2. Solving received challenge
	conf := ctx.Value(cntxt.Config).(config.App)
	solvedChallengeData, err := msg.ChallengeData.SolveChallenge(conf.MaxIterations)
	if err != nil {
		c.logger.WithError(err).WithField("step", 2).Error("failed to solve challenge")
		return "", err
	}

	c.logger.WithFields(log.Fields{"solvedChallenge": solvedChallengeData, "step": 2}).Info("ChallengeData solved")

	// Step 3. Send solved challenge to server
	answer := protocol.Message{
		MessageType:   protocol.RequestAnswer,
		ChallengeData: solvedChallengeData,
	}
	marshalledAnswer, err := json.Marshal(answer)
	if err != nil {
		c.logger.WithError(err).WithField("step", 3).Error("failed to marshal solved challenge")
		return "", err
	}

	err = c.sendMsg(marshalledAnswer, conWriter)
	if err != nil {
		c.logger.WithError(err).WithField("step", 3).Error("failed send answer")
		return "", err
	}

	c.logger.WithFields(log.Fields{"step": 3}).Info("Solved challenge sent to server")

	// Step 4. Accept from server with quote or err
	msgStr, err = c.readConnMsg(reader)
	if err != nil {
		c.logger.WithError(err).WithField("step", 4).Error("failed to read server answer for solved challenge")
		return "", err
	}

	msgGrant := protocol.Message{}
	err = json.Unmarshal([]byte(msgStr), &msgGrant)
	if err != nil {
		c.logger.WithError(err).WithField("step", 4).Error("failed to unmarshal server answer for solved challenge")
		return "", err
	}

	return msgGrant.Quote, nil
}

func (c *Client) readConnMsg(reader *bufio.Reader) (string, error) {
	return reader.ReadString('\n')
}

func (c *Client) sendMsg(msg []byte, conn io.Writer) error {
	msg = append(msg, '\n')
	_, err := conn.Write(msg)
	return err
}
