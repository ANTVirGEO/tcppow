package server

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRepository_getAllQuotes(t *testing.T) {
	repo := NewQuotesRepository()

	quotes := repo.getAllQuotes()

	assert.IsType(t, []string{}, quotes)
}
