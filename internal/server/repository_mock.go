package server

type QuotesRepositoryMock struct {
}

func NewQuotesRepositoryMock() *QuotesRepositoryMock {
	return &QuotesRepositoryMock{}
}

func (repo *QuotesRepositoryMock) getAllQuotes() []string {
	return []string{
		"Boldness be my friend. Arm me, audacity, from head to foot. - Emperor",
	}
}
