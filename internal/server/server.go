package server

import (
	"bufio"
	"context"
	"encoding/base64"
	"encoding/json"
	"github.com/antvirgeo/tcp-pow/config"
	"github.com/antvirgeo/tcp-pow/pkg/cache"
	"github.com/antvirgeo/tcp-pow/pkg/context"
	"github.com/antvirgeo/tcp-pow/pkg/customerrors"
	"github.com/antvirgeo/tcp-pow/pkg/pow"
	"github.com/antvirgeo/tcp-pow/pkg/protocol"
	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"math/rand"
	"net"
	"time"
)

type Server struct {
	logger *log.Entry
	repo   RepoInterface
}

func NewServer(
	repo RepoInterface,
) *Server {
	return &Server{
		logger: log.WithField("component", "server"),
		repo:   repo,
	}
}

//TODO: probably best way to done this was via middleware, cause it is not business logic and it is not a good place for it in internal

// Run server to listen and handle clients incoming connections
func (s *Server) Run(ctx context.Context, address string) error {
	listener, err := net.Listen("tcp", address)
	if err != nil {
		s.logger.WithError(err).WithField("address", address).
			Error("failed server to start listen")
		return err
	}
	defer listener.Close()

	s.logger.WithFields(log.Fields{"address": address}).Info("server start listen")

	for {
		conn, err := listener.Accept()
		if err != nil {
			s.logger.WithError(err).Error("failed to accept client connection")
			return err
		}

		go s.handleConnection(ctx, conn)
	}
}

func (s *Server) handleConnection(ctx context.Context, conn net.Conn) {
	logger := s.logger.WithFields(log.Fields{"address": conn.RemoteAddr()})
	logger.Debug("handle new client connection")

	defer conn.Close()

	//TODO: was thinking about wrap all messages to base64 with some hash with salt for additional security,
	//but it is probably overkill for current stage of development, wrote it for future improve

	reader := bufio.NewReader(conn)

	for {
		req, err := reader.ReadString('\n')
		if err != nil {
			if err.Error() == "EOF" {
				logger.WithError(err).Debug("Connection closed by client")
				continue
			} else {
				logger.WithError(err).Error("failed to read from client connection")
				return
			}
		}

		msg, err := s.ProcessRequest(ctx, req, conn.RemoteAddr().String(), "")
		if err != nil {
			errMsg := "failed to process client request"
			switch err {
			case customerrors.ErrSrvInvalid,
				customerrors.ErrSrvChallengeExpiredOrNotSent,
				customerrors.ErrSrvUnknownHeader,
				customerrors.ErrPOWMaxIterations:
				logger.WithError(err).WithFields(log.Fields{"reason": err.Error()}).
					Warn(errMsg)
			default:
				logger.WithError(err).WithFields(log.Fields{"reason": "unexpected"}).
					Error(errMsg)
			}

			return
		}

		if msg != nil {
			marshaledMsg, err := json.Marshal(msg)
			if err != nil {
				logger.WithError(err).WithField("msg", msg).
					Error("failed to marshal message for client")
				return
			}

			err = s.sendAnswerToClient(marshaledMsg, conn)
			if err != nil {
				logger.WithError(err).Error("failed to send message")
			}
		}
	}
}

func (s *Server) ProcessRequest(ctx context.Context, msgStr, clientAddress, signature string) (*protocol.Message, error) {
	logger := s.logger.WithFields(log.Fields{"address": clientAddress})

	msg := protocol.Message{}
	err := json.Unmarshal([]byte(msgStr), &msg)
	if err != nil {
		logger.WithError(err).WithField("msgStr", msgStr).Error("failed to unmarshal client message")
		return nil, err
	}

	conf := ctx.Value(cntxt.Config).(config.App)
	cacheRedis := ctx.Value(cntxt.Cache).(cache.Cache)

	switch msg.MessageType {
	case protocol.Exit:
		return nil, nil
	case protocol.RequestChallenge:
		logger.Debug("client requests challenge")

		// client signature to cache with expired possibility for RequestAnswer stage
		if signature == "" {
			signature = uuid.New().String()
		}
		err = cacheRedis.Add(signature, conf.Duration)
		if err != nil {
			logger.WithError(err).Error("failed to add signature to cache")
			return nil, err
		}

		payload := pow.ChallengeData{
			Version:    1,
			ZerosCount: conf.ZerosCount,
			Date:       time.Now().Unix(),
			Resource:   clientAddress,
			Signature:  base64.StdEncoding.EncodeToString([]byte(signature)),
			Counter:    0,
		}

		answer := protocol.Message{
			MessageType:   protocol.ResponseChallenge,
			ChallengeData: payload,
		}

		return &answer, nil
	case protocol.RequestAnswer:
		logger.WithField("payload", msg.ChallengeData).Debug("client answer challenge")

		// check client has same address
		if msg.ChallengeData.Resource != clientAddress {
			return nil, customerrors.ErrSrvInvalid
		}

		// decoding rand from base64 field in received client's hashcash
		signatureText, err := base64.StdEncoding.DecodeString(msg.ChallengeData.Signature)
		if err != nil {
			logger.WithError(err).WithField("randValue", msg.ChallengeData.Signature).
				Error("failed to decode rand from base64")
			return nil, err
		}

		signature, err := uuid.Parse(string(signatureText))
		if err != nil {
			logger.WithError(err).Error("failed parse client signature to uuid")
			return nil, err
		}

		// check client signature if cache for previous client session from RequestChallenge
		exists, err := cacheRedis.Get(signature.String())
		if err != nil {
			logger.WithError(err).WithField("signature", signature.String()).
				Error("failed get client signature from cache")
			return nil, err
		}
		if !exists {
			return nil, customerrors.ErrSrvChallengeExpiredOrNotSent
		}

		// validate client count min|max iterations limits
		maxIter := msg.ChallengeData.Counter
		if maxIter <= 0 || maxIter > conf.MaxIterations {
			logger.WithField("clientCounter", maxIter).
				Warn("attention! some strange Counter from client")
			maxIter = 1
		}

		_, err = msg.ChallengeData.SolveChallenge(maxIter)
		if err != nil {
			return nil, err
		}

		logger.WithField("payload", msg.ChallengeData).Debug("client complete challenge")

		answer := protocol.Message{
			MessageType: protocol.ResponseAnswer,
			Quote:       s.getRandomQuote(ctx),
		}

		// flush cache client signature due the end of verification cycle
		cacheRedis.Delete(signature.String())

		return &answer, nil
	default:
		return nil, customerrors.ErrSrvUnknownHeader
	}
}

func (s *Server) sendAnswerToClient(msg []byte, conn net.Conn) error {
	msg = append(msg, '\n')
	_, err := conn.Write(msg)
	return err
}

func (s *Server) getRandomQuote(ctx context.Context) string {
	quotes := s.repo.getAllQuotes()
	rnd := ctx.Value(cntxt.Random).(*rand.Rand)
	randomIndex := rnd.Intn(len(quotes))

	return quotes[randomIndex]
}
