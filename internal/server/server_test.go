package server

import (
	"context"
	"encoding/base64"
	"fmt"
	"github.com/antvirgeo/tcp-pow/config"
	"github.com/antvirgeo/tcp-pow/pkg/cache"
	cntxt "github.com/antvirgeo/tcp-pow/pkg/context"
	"github.com/antvirgeo/tcp-pow/pkg/customerrors"
	"github.com/antvirgeo/tcp-pow/pkg/pow"
	"github.com/antvirgeo/tcp-pow/pkg/protocol"
	"github.com/stretchr/testify/assert"
	"math/rand"
	"testing"
	"time"
)

func getMockServer() (*Server, *QuotesRepository, context.Context) {
	cfg := config.Read()

	// random form quotes (rand.Seed deprecated in 1.20)
	seed := rand.NewSource(time.Now().UnixNano())
	rnd := rand.New(seed)

	ctx := context.Background()
	ctx = context.WithValue(ctx, cntxt.Config, cfg.App)
	ctx = context.WithValue(ctx, cntxt.Random, rnd)
	ctx = context.WithValue(ctx, cntxt.Cache, cache.NewMockCache())

	repo := NewQuotesRepository()

	return NewServer(repo), repo, ctx
}

type TestCheckData struct {
	ClientMessage string
	ClientAddress string
	Completed     bool
	Positive      bool
	Description   string
	ExpectedError error
}

func TestProcessRequest(t *testing.T) {
	srv, _, ctx := getMockServer()

	conf := ctx.Value(cntxt.Config).(config.App)

	const clientSignature = "6b6e57f8-b5f1-421c-8c06-cffb1511f2b8"

	var testData = []TestCheckData{
		{
			Description:   "Bad json client message",
			ClientMessage: "}DEFINITELY_NO_A_JSON{",
			Positive:      false,
		},
		{
			Description:   "Exit must be wo error",
			ClientMessage: fmt.Sprintf(`{"header":%d}`, protocol.Exit),
			Positive:      true,
		},
		{
			Description:   "Exit must be wo error",
			ClientMessage: fmt.Sprintf(`{"header":%d}`, 9000),
			Positive:      false,
			ExpectedError: customerrors.ErrSrvUnknownHeader,
		},
		//step 2
		{
			Description:   "Step 2. Challenge answer with wrong client",
			ClientMessage: fmt.Sprintf(`{"header":%d}`, protocol.RequestAnswer),
			ClientAddress: "vah",
			Positive:      false,
			ExpectedError: customerrors.ErrSrvInvalid,
		},
		{
			Description:   "Step 2. Challenge answer with bag not base 64 signature",
			ClientMessage: fmt.Sprintf(`{"header":%d,"challenge_data":{"Signature":"ABRVAL"}}`, protocol.RequestAnswer),
			Positive:      false,
		},
		{
			Description:   "Step 2. Challenge answer with bad not uuid signature",
			ClientMessage: fmt.Sprintf(`{"header":%d,"challenge_data":{"Signature":"QUJSVkFM"}}`, protocol.RequestAnswer),
			Positive:      false,
		},
		{
			Description:   "Step 2. Challenge answer with bad time",
			ClientMessage: fmt.Sprintf(`{"header":%d,"challenge_data":{"Version":1,"ZerosCount":3,"Date":1691160100,"Resource":"127.0.0.1:54313","Signature":"NzE2OWExZjYtMmUyMS00MTgwLWE4OTUtYzJiNzk3NjliMTk1","Counter":1059}}`, protocol.RequestAnswer),
			ClientAddress: "127.0.0.1:54313",
			Positive:      false,
			ExpectedError: customerrors.ErrPOWMaxIterations,
		},
		{
			Description:   "Step 2. Challenge answer with less iterations that needed",
			ClientMessage: fmt.Sprintf(`{"header":%d,"challenge_data":{"Version":1,"ZerosCount":3,"Date":1691161047,"Resource":"127.0.0.1:54791","Signature":"YTA5ODM4YWItM2M2My00NTI4LTg0ZTgtZGQzYzJlOTljN2Q2","Counter":2285}}`, protocol.RequestAnswer),
			ClientAddress: "127.0.0.1:54791",
			Positive:      false,
			ExpectedError: customerrors.ErrPOWMaxIterations,
		},
		{
			Description:   "Step 2. Challenge answer with more zero count that max iterations can handle",
			ClientMessage: fmt.Sprintf(`{"header":%d,"challenge_data":{"Version":1,"ZerosCount":4,"Date":1691161047,"Resource":"127.0.0.1:54791","Signature":"YTA5ODM4YWItM2M2My00NTI4LTg0ZTgtZGQzYzJlOTljN2Q2","Counter":2287}}`, protocol.RequestAnswer),
			ClientAddress: "127.0.0.1:54791",
			Positive:      false,
			ExpectedError: customerrors.ErrPOWMaxIterations,
		},
		{
			Description:   "Step 2. Challenge answer full success",
			ClientMessage: fmt.Sprintf(`{"header":%d,"challenge_data":{"Version":1,"ZerosCount":3,"Date":1691161047,"Resource":"127.0.0.1:54791","Signature":"YTA5ODM4YWItM2M2My00NTI4LTg0ZTgtZGQzYzJlOTljN2Q2","Counter":2287}}`, protocol.RequestAnswer),
			ClientAddress: "127.0.0.1:54791",
			Positive:      true,
		},
	}

	for _, test := range testData {
		msgOnError := fmt.Sprintf("Description: %v;\nClientMessage: %v;\nPositive: %v;\n",
			test.Description, test.ClientMessage, test.Positive)
		_, err := srv.ProcessRequest(ctx, test.ClientMessage, test.ClientAddress, clientSignature)
		if test.Positive {
			assert.NoError(t, err, msgOnError)
		} else {
			assert.Error(t, err, msgOnError)
			if test.ExpectedError != nil {
				assert.ErrorIs(t, err, test.ExpectedError)
			}
		}
	}

	// test for initial challenge logic
	test := TestCheckData{
		Description:   "Step 1. Successful challenge request",
		ClientMessage: fmt.Sprintf(`{"header":%d}`, protocol.RequestChallenge),
		ClientAddress: "",
	}
	msgOnError := fmt.Sprintf("Description: %v;\nClientMessage: %v;\nPositive: %v;\n",
		test.Description, test.ClientMessage, test.Positive)
	result, err := srv.ProcessRequest(ctx, test.ClientMessage, test.ClientAddress, clientSignature)
	assert.NoError(t, err, msgOnError)

	//possible problem with Date, mb need to transfer custom date for tests
	payload := pow.ChallengeData{
		Version:    1,
		ZerosCount: conf.ZerosCount,
		Date:       time.Now().Unix(),
		Resource:   test.ClientAddress,
		Signature:  base64.StdEncoding.EncodeToString([]byte(clientSignature)),
		Counter:    0,
	}

	answer := protocol.Message{
		MessageType:   protocol.ResponseChallenge,
		ChallengeData: payload,
	}

	assert.Equal(t, result, &answer)
}
