package server

//TODO: There can be possible DB repo

type QuotesRepository struct {
	Quotes []string
}

func NewQuotesRepository() *QuotesRepository {
	return &QuotesRepository{
		Quotes: data,
	}
}

// A little bit useless with statis data, but if we have db here in a future...
func (repo *QuotesRepository) getAllQuotes() []string {
	return repo.Quotes
}

// Quotes - some good quotes to respond on client's request (no repository for this task)
var data = []string{
	"Boldness be my friend. Arm me, audacity, from head to foot. - Emperor",
	"A wise man trusts no one. - Roboute Guilliman",
	"For the greater good, death is a small price to pay. - Kais Kain",
	"Blessed is the mind too small for doubt. - Leman Russ",
	"The sleep of reason produces monsters. - Emperor",
	"Only a fool believes he is anything without fear. And having no fear, he believes he knows everything. - Leman Russ",
	"We are the Legion of the Damned. We are the ocean's fury. We are the end. - Legion of the Damned",
	"War is our love and our adventure. - Macharius",
	"Sshchhhhh? Sshsschsshsshssch! - Some random necron warrior",
}
